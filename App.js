/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import * as React from 'react';
import { Routes } from './src/routes';


/* Redux Saga Setup */
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { reducers, rootSaga } from './src/redux';
import { Provider } from 'react-redux';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// mount it on the Store
const store = createStore(
  reducers,
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);
/* Redux Saga Setup */

const App = () => {
  return (
    <Provider store={store}>
      <Routes />
      {/* <Text></Text> */}
    </Provider>
  );
};

export default App;
