import * as login from './login';
import * as product from './product';
// import * as forgotPassword from './forgotPassword';
// import * as resetPassword from './resetPassword';
// import * as signUp from './signUp';
// import * as userProfile from './user';
// import * as privacyPolicy from './privacyPolicyAction';

export {
    login,
    product
    // forgotPassword,
    // resetPassword,
    // signUp,
    // userProfile,
    // privacyPolicy
};
