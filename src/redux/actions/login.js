export const SUBMIT_LOGIN_PARAMS = 'SUBMIT_LOGIN_PARAMS';
export const SET_LOGIN_STATUS = 'SET_LOGIN_STATUS';


export const submitLoginParams = loginParams => ({
  type: SUBMIT_LOGIN_PARAMS,
  loginParams,
});

/* SAMPLE DATA
export const submitLoginParams = true => ({
  type: SUBMIT_LOGIN_PARAMS,
  true,
});
*/

export const setLoginStatus = loginStatusParams => ({
  type: SET_LOGIN_STATUS,
  loginStatusParams,
});