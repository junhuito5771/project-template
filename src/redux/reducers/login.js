import {
    SET_LOGIN_LOADING,
    SET_LOGIN_STATUS,
} from '../actions/login';

const initialState = {
  isLoading: false,
  isLogin: false,
  error: '',
  
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOGIN_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_LOGIN_STATUS: {
      return {
        ...state,
        isLogin: action.loginStatusParams
      };
    }
    default:
      return state;
  }
};
  
export default reducer;