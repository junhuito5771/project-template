import {
    SET_PRODUCT_DATA,
} from '../actions/login';

const initialState = {
    data:{},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCT_DATA:
      return {
        ...state,
        data:{
            ...state.data,
            ...action.data
        }
      };
    default:
      return state;
  }
};
  
export default reducer;