import { all, fork } from 'redux-saga/effects';

import LoginSaga from './login';
// import AutoLoginSaga from './autoLogin';

export default function* rootSaga() {
  yield all([
    fork(LoginSaga),
    // fork(AutoLoginSaga),
  ]);
}