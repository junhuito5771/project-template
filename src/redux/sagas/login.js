import { call, put, takeEvery, takeLatest, takeLeading } from 'redux-saga/effects';

import {
  setLoginStatus,
  SUBMIT_LOGIN_PARAMS,
} from '../actions/login';

function* submitLoginForm(action) {

    /* Sample Data
    action.loginParams = {
        'username':'sunny5771@hotmail.com',
        'password':'Test1234'
    }
    */

    console.log("submitLoginForm");

    // do some api call
    const loginSuccess = action.loginParams;

    /*
    const data = yield call(APIService.post, {
      'username':'sunny5771@hotmail.com',
        'password':'Test1234'
    })
    */

    // if login success, set login status
    yield put(setLoginStatus(loginSuccess));

}

export default function* root() {
  yield takeLatest(SUBMIT_LOGIN_PARAMS, submitLoginForm);
}