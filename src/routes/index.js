import { NavigationContainer } from "@react-navigation/native";

import { StackRoutes } from "./stack";
import { TabsRoutes } from "./tabs";
import { DrawerRoutes } from "./drawer";
import React from 'react';


export function Routes() {
    return (
        <NavigationContainer>
            {/* <StackRoutes /> */}
            {/* <TabsRoutes /> */}
            <DrawerRoutes />
        </NavigationContainer>
    )
}