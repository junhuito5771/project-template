import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

import { styles } from './styles';

import { connect } from 'react-redux';
import { login } from '../../redux/actions';

const ScreenA = ({ navigation, isLogin, functionNameA})=>{

    function openScreen() {
        navigation.navigate('screenB', {
            itemId: 86,
            name: 'Juliano Gaspar',
        })
    }

    return (
        <View style={styles.screen}>
            <Text style={styles.text}>  
                Screen A
            </Text>
            <Text style={styles.text}>
                {JSON.stringify(isLogin)}
            </Text>
            <Button
                title="To Screen B"
                onPress={openScreen}
            />

            <Button
                title="Login"
                onPress={()=>{
                    functionNameA(true);
                }}
            />

            <Button
                title="Logout"
                onPress={()=>{
                    functionNameA(false);
                }}
            />
        </View>
    );
}

const mapStateToProps = (state)=>({
    isLogin: state.login.isLogin
});

const mapDispatchToProps = dispatch => ({   
    functionNameA: value => {
        console.log("login value" + value);
        dispatch(login.submitLoginParams(value));
    },
});

export default connect(mapStateToProps,mapDispatchToProps)(ScreenA);