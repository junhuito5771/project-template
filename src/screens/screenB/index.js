import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'lightblue',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 56,
        color: 'white'
    }
});

const ScreenB = ({ route, navigation }) => {

    // const { itemId, name } = route.params;

    return (
        <View style={styles.screen}>
            <Text style={styles.text}>Screen B</Text>
            {/* <Text>Id: {JSON.stringify('itemId')}</Text>
            <Text>Nome: {JSON.stringify('name')}</Text> */}

            <Button title='Go Back Screen A'
                onPress={() => navigation.goBack()} />
        </View>
    );
}

export default ScreenB;